# Quick overview #
This is implemented as an Azure Function:
* calc/HttpTriggerCalc.cs is the function wrapper
* calc/Calc.cs implements the core functionality

Unit tests are in located in:
* calc.Tests/UnitTest1.cs

Sample request:
```
curl --location --request POST 'https://ewen-calc-fn.azurewebsites.net/api/HttpTriggerCalc?code=<insert authorization code>' \
--header 'Content-Type: text/plain' \
--data-raw 'ADD 5
MULTIPLY 50
DIVIDE 4
APPLY 5
'
```