using System;
using System.Collections.Generic;
using NUnit.Framework;
using calc;

namespace calc.Tests
{
    public class Tests
    {
        [TestCase("ADD 5\r\nSUBTRACT 1\r\nMULTIPLY 2\r\nAPPLY 2", 12)]
        [TestCase("ADD 10\r\nDIVIDE 3\r\nMULTIPLY 2\r\nAPPLY 5", 10)]
        public void Test_validInstructions(string rawInstructions, decimal expectedAnswer)
        {
            List<string> instructions = GetList(rawInstructions);

            var result = Calc.instructionsAreValid(instructions);
            Assert.IsTrue(result);

            var answer = Calc.executeInstructions(instructions);
            Assert.AreEqual(answer, expectedAnswer);
        }

        [TestCase("BOB 5\r\nSUBTRACT 1\r\nMULTIPLY 2\r\nAPPLY 2")] // invalid instruction
        [TestCase("ADD 10\r\nDIVIDE 0\r\nMULTIPLY 2\r\nAPPLY 5")] // divide by zero
        [TestCase("ADD 10\r\nADD 2\r\nAPPLY 5\r\nMULTIPLY 2")] // APPLY not last instruction
        public void Test_invalidInstructions(string rawInstructions)
        {
            List<string> instructions = GetList(rawInstructions);

            var result = Calc.instructionsAreValid(instructions);
            Assert.IsFalse(result, $"{instructions} are invalid");
        }

        private List<string> GetList(string rawInstructions) {
            return Calc.cleanseInstructions(
                new List<string>(rawInstructions.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None
            )));
        }
    }
}