using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace calc {
    public static class Calc
    {
        public static string doCalc(string rawInstructions) 
        {
            string responseMessage = "";

            // split the request body into a list
            List<string> instructions = Calc.cleanseInstructions(
                new List<string>(rawInstructions.Split(
                    new[] { Environment.NewLine },
                    StringSplitOptions.None
            )));

            string strInstructions = String.Join("\n", instructions.ToArray());
            if (Calc.instructionsAreValid(instructions)) 
            {
                responseMessage +=
                    "\n" + strInstructions
                    + "\nAnswer: " + Calc.executeInstructions(instructions).ToString();
            }
            else
            {
                responseMessage +=
                    "/n Invalid set of instructions - APPLY must be on the last line; other lines must have [ADD|SUBTRACT|MULTIPLE|DIVIDE] <space> number."
                    + "\n" + strInstructions;
            }

            return responseMessage;
        }

        public static List<string> cleanseInstructions(List<string> instructions)
        {
            // remove empty/whitespace only strings; trim the remaining ones
            instructions = instructions.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
            instructions = instructions.Select(s => s.Trim()).ToList();
            return instructions;
        }

        public static bool instructionsAreValid(List<string> instructions)
        {
            // validate instructions
            Regex rgx = new Regex(@"^\b(APPLY|ADD|SUBTRACT|MULTIPLY|DIVIDE)\b [0-9]*$");
            int totalCount = instructions.Count;

            for (int count = 0; count < totalCount; count++)
            {
                string instruction = instructions[count];
                if (!rgx.IsMatch(instruction)
                    || (instruction.StartsWith("DIVIDE") && Regex.Match(instructions[count], @"\d+").Value == "0")
                    || ((count + 1) == totalCount && !instruction.StartsWith("APPLY")))
                {
                    // line does not contain valid instruction and a number
                    // or line contains divide by zero
                    // or last line does not contain APPLY
                    return false;
                }
            }
            return true;
        }

        public static decimal executeInstructions(List<string> instructions) {
            int totalCount = instructions.Count;
            decimal answer = Convert.ToDecimal(
                Regex.Match(instructions[totalCount-1], @"\d+").Value);
            
            for (int count = 0; count < totalCount; count++)
            {
                if ((count + 1) != totalCount) 
                {
                    decimal number = Convert.ToDecimal(
                        Regex.Match(instructions[count], @"\d+").Value);

                    string whatToDo = Regex.Match(instructions[count], @"[A-Z]+").Value;
                    switch (whatToDo) 
                    {
                        case "ADD":
                            answer += number;
                            break;
                        case "SUBTRACT":
                            answer -= number;
                            break;
                        case "MULTIPLY":
                            answer = answer * number;
                            break;
                        case "DIVIDE":
                            answer = answer / number;
                            break;
                    }
                }
            }

            return answer;
        }
    }
}